package com.evgenbondarenko.air_sensor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.github.mikephil.charting.charts.LineChart;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentChartBinding extends ViewDataBinding {
  @NonNull
  public final LineChart chart1;

  @NonNull
  public final SeekBar seekBar1;

  @NonNull
  public final SeekBar seekBar2;

  @NonNull
  public final TextView tvXMax;

  @NonNull
  public final TextView tvYMax;

  protected FragmentChartBinding(Object _bindingComponent, View _root, int _localFieldCount,
      LineChart chart1, SeekBar seekBar1, SeekBar seekBar2, TextView tvXMax, TextView tvYMax) {
    super(_bindingComponent, _root, _localFieldCount);
    this.chart1 = chart1;
    this.seekBar1 = seekBar1;
    this.seekBar2 = seekBar2;
    this.tvXMax = tvXMax;
    this.tvYMax = tvYMax;
  }

  @NonNull
  public static FragmentChartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_chart, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentChartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentChartBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.fragment_chart, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentChartBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_chart, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentChartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentChartBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.fragment_chart, null, false, component);
  }

  public static FragmentChartBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentChartBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentChartBinding)bind(component, view, com.evgenbondarenko.air_sensor.R.layout.fragment_chart);
  }
}
