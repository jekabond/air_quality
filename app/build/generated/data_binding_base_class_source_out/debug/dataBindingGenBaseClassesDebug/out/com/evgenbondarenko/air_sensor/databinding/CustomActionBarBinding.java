package com.evgenbondarenko.air_sensor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class CustomActionBarBinding extends ViewDataBinding {
  @NonNull
  public final AppCompatTextView actionBarText;

  @NonNull
  public final AppCompatImageView iconGps;

  @NonNull
  public final AppCompatImageView iconInternet;

  @NonNull
  public final AppCompatImageButton menuButton;

  protected CustomActionBarBinding(Object _bindingComponent, View _root, int _localFieldCount,
      AppCompatTextView actionBarText, AppCompatImageView iconGps, AppCompatImageView iconInternet,
      AppCompatImageButton menuButton) {
    super(_bindingComponent, _root, _localFieldCount);
    this.actionBarText = actionBarText;
    this.iconGps = iconGps;
    this.iconInternet = iconInternet;
    this.menuButton = menuButton;
  }

  @NonNull
  public static CustomActionBarBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.custom_action_bar, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static CustomActionBarBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<CustomActionBarBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.custom_action_bar, root, attachToRoot, component);
  }

  @NonNull
  public static CustomActionBarBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.custom_action_bar, null, false, component)
   */
  @NonNull
  @Deprecated
  public static CustomActionBarBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<CustomActionBarBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.custom_action_bar, null, false, component);
  }

  public static CustomActionBarBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static CustomActionBarBinding bind(@NonNull View view, @Nullable Object component) {
    return (CustomActionBarBinding)bind(component, view, com.evgenbondarenko.air_sensor.R.layout.custom_action_bar);
  }
}
