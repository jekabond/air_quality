package com.evgenbondarenko.air_sensor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainContract;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityMainBinding extends ViewDataBinding {
  @NonNull
  public final BottomNavigationView bottomNavigation;

  @NonNull
  public final ConstraintLayout contentMain;

  @NonNull
  public final CustomActionBarBinding customActionBar;

  @NonNull
  public final ConstraintLayout mainLayout;

  @Bindable
  protected MainContract.Presenter mEvent;

  protected ActivityMainBinding(Object _bindingComponent, View _root, int _localFieldCount,
      BottomNavigationView bottomNavigation, ConstraintLayout contentMain,
      CustomActionBarBinding customActionBar, ConstraintLayout mainLayout) {
    super(_bindingComponent, _root, _localFieldCount);
    this.bottomNavigation = bottomNavigation;
    this.contentMain = contentMain;
    this.customActionBar = customActionBar;
    setContainedBinding(this.customActionBar);;
    this.mainLayout = mainLayout;
  }

  public abstract void setEvent(@Nullable MainContract.Presenter event);

  @Nullable
  public MainContract.Presenter getEvent() {
    return mEvent;
  }

  @NonNull
  public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_main, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityMainBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.activity_main, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_main, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityMainBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.activity_main, null, false, component);
  }

  public static ActivityMainBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityMainBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityMainBinding)bind(component, view, com.evgenbondarenko.air_sensor.R.layout.activity_main);
  }
}
