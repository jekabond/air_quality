package com.evgenbondarenko.air_sensor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.progress.progressview.ProgressView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentSensorsBinding extends ViewDataBinding {
  @NonNull
  public final View divider;

  @NonNull
  public final View divider2;

  @NonNull
  public final Guideline guideBot;

  @NonNull
  public final Guideline guideLeft;

  @NonNull
  public final Guideline guideRight;

  @NonNull
  public final Guideline guideTop;

  @NonNull
  public final ProgressView pm10Progress;

  @NonNull
  public final AppCompatTextView pm10Title;

  @NonNull
  public final AppCompatTextView pm10Value;

  @NonNull
  public final ProgressView pm25Progress;

  @NonNull
  public final AppCompatTextView pm25Title;

  @NonNull
  public final AppCompatTextView pm25Value;

  @NonNull
  public final ProgressView pm5Progress;

  @NonNull
  public final AppCompatTextView pm5Title;

  @NonNull
  public final AppCompatTextView pm5Value;

  protected FragmentSensorsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      View divider, View divider2, Guideline guideBot, Guideline guideLeft, Guideline guideRight,
      Guideline guideTop, ProgressView pm10Progress, AppCompatTextView pm10Title,
      AppCompatTextView pm10Value, ProgressView pm25Progress, AppCompatTextView pm25Title,
      AppCompatTextView pm25Value, ProgressView pm5Progress, AppCompatTextView pm5Title,
      AppCompatTextView pm5Value) {
    super(_bindingComponent, _root, _localFieldCount);
    this.divider = divider;
    this.divider2 = divider2;
    this.guideBot = guideBot;
    this.guideLeft = guideLeft;
    this.guideRight = guideRight;
    this.guideTop = guideTop;
    this.pm10Progress = pm10Progress;
    this.pm10Title = pm10Title;
    this.pm10Value = pm10Value;
    this.pm25Progress = pm25Progress;
    this.pm25Title = pm25Title;
    this.pm25Value = pm25Value;
    this.pm5Progress = pm5Progress;
    this.pm5Title = pm5Title;
    this.pm5Value = pm5Value;
  }

  @NonNull
  public static FragmentSensorsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_sensors, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentSensorsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentSensorsBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.fragment_sensors, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentSensorsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_sensors, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentSensorsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentSensorsBinding>inflateInternal(inflater, com.evgenbondarenko.air_sensor.R.layout.fragment_sensors, null, false, component);
  }

  public static FragmentSensorsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentSensorsBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentSensorsBinding)bind(component, view, com.evgenbondarenko.air_sensor.R.layout.fragment_sensors);
  }
}
