// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.presentation.fragments.chart;

import dagger.internal.Factory;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ChartPresenter_Factory implements Factory<ChartPresenter> {
  private static final ChartPresenter_Factory INSTANCE = new ChartPresenter_Factory();

  @Override
  public ChartPresenter get() {
    return new ChartPresenter();
  }

  public static ChartPresenter_Factory create() {
    return INSTANCE;
  }

  public static ChartPresenter newInstance() {
    return new ChartPresenter();
  }
}
