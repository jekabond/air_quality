// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.di.sing_module;

import android.content.Context;
import com.evgenbondarenko.air_sensor.App;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideContextFactory implements Factory<Context> {
  private final Provider<App> appProvider;

  public AppModule_ProvideContextFactory(Provider<App> appProvider) {
    this.appProvider = appProvider;
  }

  @Override
  public Context get() {
    return provideContext(appProvider.get());
  }

  public static AppModule_ProvideContextFactory create(Provider<App> appProvider) {
    return new AppModule_ProvideContextFactory(appProvider);
  }

  public static Context provideContext(App app) {
    return Preconditions.checkNotNull(AppModule.provideContext(app), "Cannot return null from a non-@Nullable @Provides method");
  }
}
