// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.presentation.fragments.settings;

import dagger.MembersInjector;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SettingsPresenter_MembersInjector implements MembersInjector<SettingsPresenter> {
  private final Provider<SettingsContract.View> viewProvider;

  public SettingsPresenter_MembersInjector(Provider<SettingsContract.View> viewProvider) {
    this.viewProvider = viewProvider;
  }

  public static MembersInjector<SettingsPresenter> create(
      Provider<SettingsContract.View> viewProvider) {
    return new SettingsPresenter_MembersInjector(viewProvider);}

  @Override
  public void injectMembers(SettingsPresenter instance) {
    injectView(instance, viewProvider.get());
  }

  public static void injectView(SettingsPresenter instance, SettingsContract.View view) {
    instance.view = view;
  }
}
