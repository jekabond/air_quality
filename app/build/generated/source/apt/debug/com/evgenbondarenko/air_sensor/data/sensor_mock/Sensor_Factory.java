// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.data.sensor_mock;

import dagger.internal.Factory;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class Sensor_Factory implements Factory<Sensor> {
  private static final Sensor_Factory INSTANCE = new Sensor_Factory();

  @Override
  public Sensor get() {
    return new Sensor();
  }

  public static Sensor_Factory create() {
    return INSTANCE;
  }

  public static Sensor newInstance() {
    return new Sensor();
  }
}
