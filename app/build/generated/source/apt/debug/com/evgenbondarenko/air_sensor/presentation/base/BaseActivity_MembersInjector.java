// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.presentation.base;

import androidx.databinding.ViewDataBinding;
import dagger.MembersInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.DaggerAppCompatActivity_MembersInjector;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BaseActivity_MembersInjector<Binding extends ViewDataBinding> implements MembersInjector<BaseActivity<Binding>> {
  private final Provider<DispatchingAndroidInjector<Object>> androidInjectorProvider;

  public BaseActivity_MembersInjector(
      Provider<DispatchingAndroidInjector<Object>> androidInjectorProvider) {
    this.androidInjectorProvider = androidInjectorProvider;
  }

  public static <Binding extends ViewDataBinding> MembersInjector<BaseActivity<Binding>> create(
      Provider<DispatchingAndroidInjector<Object>> androidInjectorProvider) {
    return new BaseActivity_MembersInjector<Binding>(androidInjectorProvider);}

  @Override
  public void injectMembers(BaseActivity<Binding> instance) {
    DaggerAppCompatActivity_MembersInjector.injectAndroidInjector(instance, androidInjectorProvider.get());
  }
}
