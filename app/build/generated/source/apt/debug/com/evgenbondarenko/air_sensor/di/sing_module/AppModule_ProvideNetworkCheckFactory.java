// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.di.sing_module;

import com.evgenbondarenko.air_sensor.App;
import com.evgenbondarenko.air_sensor.data.network_check.INetworkCheck;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideNetworkCheckFactory implements Factory<INetworkCheck> {
  private final Provider<App> appProvider;

  public AppModule_ProvideNetworkCheckFactory(Provider<App> appProvider) {
    this.appProvider = appProvider;
  }

  @Override
  public INetworkCheck get() {
    return provideNetworkCheck(appProvider.get());
  }

  public static AppModule_ProvideNetworkCheckFactory create(Provider<App> appProvider) {
    return new AppModule_ProvideNetworkCheckFactory(appProvider);
  }

  public static INetworkCheck provideNetworkCheck(App app) {
    return Preconditions.checkNotNull(AppModule.provideNetworkCheck(app), "Cannot return null from a non-@Nullable @Provides method");
  }
}
