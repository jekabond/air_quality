package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = BuildersModuleFragment_ProvideChartFragment.ChartFragmentSubcomponent.class)
public abstract class BuildersModuleFragment_ProvideChartFragment {
  private BuildersModuleFragment_ProvideChartFragment() {}

  @Binds
  @IntoMap
  @ClassKey(ChartFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ChartFragmentSubcomponent.Factory builder);

  @Subcomponent(modules = FragmentChartModule.class)
  public interface ChartFragmentSubcomponent extends AndroidInjector<ChartFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ChartFragment> {}
  }
}
