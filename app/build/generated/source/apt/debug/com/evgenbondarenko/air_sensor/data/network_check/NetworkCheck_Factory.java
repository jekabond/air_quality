// Generated by Dagger (https://dagger.dev).
package com.evgenbondarenko.air_sensor.data.network_check;

import dagger.android.support.DaggerApplication;
import dagger.internal.Factory;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NetworkCheck_Factory implements Factory<NetworkCheck> {
  private final Provider<DaggerApplication> applicationProvider;

  public NetworkCheck_Factory(Provider<DaggerApplication> applicationProvider) {
    this.applicationProvider = applicationProvider;
  }

  @Override
  public NetworkCheck get() {
    return new NetworkCheck(applicationProvider.get());
  }

  public static NetworkCheck_Factory create(Provider<DaggerApplication> applicationProvider) {
    return new NetworkCheck_Factory(applicationProvider);
  }

  public static NetworkCheck newInstance(DaggerApplication application) {
    return new NetworkCheck(application);
  }
}
