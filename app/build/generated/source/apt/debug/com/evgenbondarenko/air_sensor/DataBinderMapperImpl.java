package com.evgenbondarenko.air_sensor;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.evgenbondarenko.air_sensor.databinding.ActivityMainBindingImpl;
import com.evgenbondarenko.air_sensor.databinding.ActivitySplashBindingImpl;
import com.evgenbondarenko.air_sensor.databinding.CustomActionBarBindingImpl;
import com.evgenbondarenko.air_sensor.databinding.FragmentChartBindingImpl;
import com.evgenbondarenko.air_sensor.databinding.FragmentSensorsBindingImpl;
import com.evgenbondarenko.air_sensor.databinding.FragmentSettingsBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_ACTIVITYSPLASH = 2;

  private static final int LAYOUT_CUSTOMACTIONBAR = 3;

  private static final int LAYOUT_FRAGMENTCHART = 4;

  private static final int LAYOUT_FRAGMENTSENSORS = 5;

  private static final int LAYOUT_FRAGMENTSETTINGS = 6;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(6);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.evgenbondarenko.air_sensor.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.evgenbondarenko.air_sensor.R.layout.activity_splash, LAYOUT_ACTIVITYSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.evgenbondarenko.air_sensor.R.layout.custom_action_bar, LAYOUT_CUSTOMACTIONBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.evgenbondarenko.air_sensor.R.layout.fragment_chart, LAYOUT_FRAGMENTCHART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.evgenbondarenko.air_sensor.R.layout.fragment_sensors, LAYOUT_FRAGMENTSENSORS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.evgenbondarenko.air_sensor.R.layout.fragment_settings, LAYOUT_FRAGMENTSETTINGS);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSPLASH: {
          if ("layout/activity_splash_0".equals(tag)) {
            return new ActivitySplashBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
        }
        case  LAYOUT_CUSTOMACTIONBAR: {
          if ("layout/custom_action_bar_0".equals(tag)) {
            return new CustomActionBarBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for custom_action_bar is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCHART: {
          if ("layout/fragment_chart_0".equals(tag)) {
            return new FragmentChartBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_chart is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSENSORS: {
          if ("layout/fragment_sensors_0".equals(tag)) {
            return new FragmentSensorsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_sensors is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSETTINGS: {
          if ("layout/fragment_settings_0".equals(tag)) {
            return new FragmentSettingsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_settings is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(3);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "event");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(6);

    static {
      sKeys.put("layout/activity_main_0", com.evgenbondarenko.air_sensor.R.layout.activity_main);
      sKeys.put("layout/activity_splash_0", com.evgenbondarenko.air_sensor.R.layout.activity_splash);
      sKeys.put("layout/custom_action_bar_0", com.evgenbondarenko.air_sensor.R.layout.custom_action_bar);
      sKeys.put("layout/fragment_chart_0", com.evgenbondarenko.air_sensor.R.layout.fragment_chart);
      sKeys.put("layout/fragment_sensors_0", com.evgenbondarenko.air_sensor.R.layout.fragment_sensors);
      sKeys.put("layout/fragment_settings_0", com.evgenbondarenko.air_sensor.R.layout.fragment_settings);
    }
  }
}
