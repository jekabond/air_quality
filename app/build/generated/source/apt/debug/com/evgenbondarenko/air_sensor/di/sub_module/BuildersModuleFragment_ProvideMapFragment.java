package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = BuildersModuleFragment_ProvideMapFragment.MapFragmentSubcomponent.class)
public abstract class BuildersModuleFragment_ProvideMapFragment {
  private BuildersModuleFragment_ProvideMapFragment() {}

  @Binds
  @IntoMap
  @ClassKey(MapFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MapFragmentSubcomponent.Factory builder);

  @Subcomponent(modules = FragmentMapModule.class)
  public interface MapFragmentSubcomponent extends AndroidInjector<MapFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<MapFragment> {}
  }
}
