package com.evgenbondarenko.air_sensor.data.room;

import androidx.room.RoomDatabase;
import java.lang.SuppressWarnings;

@SuppressWarnings({"unchecked", "deprecation"})
public final class LocalEntry_Impl implements LocalEntry {
  private final RoomDatabase __db;

  public LocalEntry_Impl(RoomDatabase __db) {
    this.__db = __db;
  }
}
