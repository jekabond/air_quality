package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = BuildersModuleFragment_ProvideSettingsFragment.SettingsFragmentSubcomponent.class
)
public abstract class BuildersModuleFragment_ProvideSettingsFragment {
  private BuildersModuleFragment_ProvideSettingsFragment() {}

  @Binds
  @IntoMap
  @ClassKey(SettingsFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SettingsFragmentSubcomponent.Factory builder);

  @Subcomponent(modules = FragmentSettingsModule.class)
  public interface SettingsFragmentSubcomponent extends AndroidInjector<SettingsFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SettingsFragment> {}
  }
}
