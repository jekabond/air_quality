package com.evgenbondarenko.air_sensor.di.sing_module;

import com.evgenbondarenko.air_sensor.di.sub_module.BuildersModuleFragment;
import com.evgenbondarenko.air_sensor.di.sub_module.MainModule;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = BuildersModuleActivity_ProvideMainActivity.MainActivitySubcomponent.class)
public abstract class BuildersModuleActivity_ProvideMainActivity {
  private BuildersModuleActivity_ProvideMainActivity() {}

  @Binds
  @IntoMap
  @ClassKey(MainActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MainActivitySubcomponent.Factory builder);

  @Subcomponent(modules = {BuildersModuleFragment.class, MainModule.class})
  public interface MainActivitySubcomponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<MainActivity> {}
  }
}
