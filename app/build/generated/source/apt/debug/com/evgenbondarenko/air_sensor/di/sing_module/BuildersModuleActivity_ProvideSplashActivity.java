package com.evgenbondarenko.air_sensor.di.sing_module;

import com.evgenbondarenko.air_sensor.di.sub_module.SplashModule;
import com.evgenbondarenko.air_sensor.presentation.activity.splash.SplashActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = BuildersModuleActivity_ProvideSplashActivity.SplashActivitySubcomponent.class
)
public abstract class BuildersModuleActivity_ProvideSplashActivity {
  private BuildersModuleActivity_ProvideSplashActivity() {}

  @Binds
  @IntoMap
  @ClassKey(SplashActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SplashActivitySubcomponent.Factory builder);

  @Subcomponent(modules = SplashModule.class)
  public interface SplashActivitySubcomponent extends AndroidInjector<SplashActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SplashActivity> {}
  }
}
