package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = BuildersModuleFragment_ProvideSensorsFragment.SensorsFragmentSubcomponent.class
)
public abstract class BuildersModuleFragment_ProvideSensorsFragment {
  private BuildersModuleFragment_ProvideSensorsFragment() {}

  @Binds
  @IntoMap
  @ClassKey(SensorsFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SensorsFragmentSubcomponent.Factory builder);

  @Subcomponent(modules = FragmentSensorsModule.class)
  public interface SensorsFragmentSubcomponent extends AndroidInjector<SensorsFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SensorsFragment> {}
  }
}
