/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.evgenbondarenko.air_sensor;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.evgenbondarenko.air_sensor";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.1";
  // Fields from build type: debug
  public static final String API_URL = "https://api.waqi.info";
  public static final String SERVER_URL = "https://127.0.0.1:1051/";
  public static final String TOKEN = "50fa4e70ab21b5ac4bc0c923c3419876b48ec457";
  public static final boolean fileDebug = true;
}
