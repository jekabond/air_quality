package com.evgenbondarenko.air_sensor.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import com.evgenbondarenko.air_sensor.BuildConfig;
import com.evgenbondarenko.air_sensor.ConstantsApp;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import timber.log.Timber;

public class FileManager {
    public static void writeFile(String funName, String log, Context context) {
        if (BuildConfig.fileDebug) {
            if (log != null && !log.isEmpty() && context != null) {
                try {
                    String privateDirPath = getPrivateCacheExternalStorageBaseDir(context);
                    File newFile = new File(privateDirPath, ConstantsApp.LOG_FILE_NAME);
                    FileWriter fw = new FileWriter(newFile, true);
                    fw.write(dateToStringConvert(new Date())
                            .concat("\n")
                            .concat(funName)
                            .concat("\n")
                            .concat(log)
                            .concat("\n")
                            .concat("\n"));
                    fw.flush();
                    fw.close();
                    Timber.e("Saved %s", getPrivateCacheExternalStorageBaseDir(context));
                } catch (Exception ex) {
                    Timber.e("Exception %s", ex.getMessage());
                }

            }
        }
    }


    private static String getPrivateCacheExternalStorageBaseDir(Context context) {
        String ret = "";
        if (isExternalStorageMounted()) {
            File file = context.getExternalCacheDir();
            ret = Objects.requireNonNull(file).getAbsolutePath();
        }
        return ret;
    }

    private static boolean isExternalStorageMounted() {
        String dirState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(dirState)) {
            return true;
        } else {
            return false;
        }
    }



    @SuppressLint("SimpleDateFormat")
    private static String dateToStringConvert(Date date) {
        Date mDate = date != null ? date : new Date();
        SimpleDateFormat dataFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        return dataFormat.format(mDate);
    }


}
