package com.evgenbondarenko.air_sensor.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;


public class CheckStatusReceiver extends BroadcastReceiver {
    private boolean work;
    private BehaviorSubject<Boolean> isGpsEnabled;
    private BehaviorSubject<Boolean> isNetworkEnabled;



    public CheckStatusReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null)
            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                if (isGpsEnabled != null)isGpsEnabled.onNext(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
                if (isNetworkEnabled != null)isNetworkEnabled.onNext(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
            }
    }

    public Observable<Boolean> getIsGpsEnabled() {
        return isGpsEnabled;
    }

    public Observable<Boolean> getIsNetworkEnabled() {
        return isNetworkEnabled;
    }

    public void onStart() {
        isGpsEnabled = BehaviorSubject.create();
        isNetworkEnabled = BehaviorSubject.create();
        work = true;
    }

    public void onStop() {
        work = false;
        isGpsEnabled.onComplete();
        isNetworkEnabled.onComplete();
        isGpsEnabled = null;
        isNetworkEnabled = null;
    }

}
