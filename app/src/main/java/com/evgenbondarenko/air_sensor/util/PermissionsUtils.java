package com.evgenbondarenko.air_sensor.util;

import android.Manifest;
import android.annotation.SuppressLint;

import com.evgenbondarenko.air_sensor.presentation.base.BaseActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;
import dagger.android.support.DaggerFragment;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PermissionsUtils {
   public String location =  Manifest.permission.ACCESS_FINE_LOCATION;

    @SuppressLint("MissingPermission")
    public static Single<Boolean> accessLocation(BaseActivity activity) {
        return new RxPermissions(activity)
                .request(Manifest.permission.ACCESS_FINE_LOCATION )
                .lastOrError();
    }

    @SuppressLint("MissingPermission")
    public static Single<Boolean> accessCamera(BaseActivity activity){
        return new RxPermissions(activity)
                .request(Manifest.permission.CAMERA)
                .lastOrError();
    }
}
