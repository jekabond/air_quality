package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.data.usb_driver.UsbDriver;
import com.evgenbondarenko.air_sensor.data.usb_driver.UsbDriverContract;
import com.evgenbondarenko.air_sensor.domain.main.MainInteractor;
import com.evgenbondarenko.air_sensor.domain.main.MainInteractorContract;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainActivity;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainContract;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {
    @Binds
    abstract MainContract.View bindMainView(MainActivity activity);

    @Binds
    abstract MainContract.Presenter bindMainPresenter(MainPresenter presenter);

    @Binds
    abstract MainInteractorContract bindSplashInteractor(MainInteractor interactor);

    @Binds
    abstract UsbDriverContract bindUsbDriver(UsbDriver driver);

}
