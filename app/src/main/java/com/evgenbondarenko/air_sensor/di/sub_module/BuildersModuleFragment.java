package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModuleFragment {
    @ContributesAndroidInjector(modules = {FragmentSettingsModule.class})
    abstract SettingsFragment provideSettingsFragment();

    @ContributesAndroidInjector(modules = {FragmentSensorsModule.class})
    abstract SensorsFragment provideSensorsFragment();

    @ContributesAndroidInjector(modules = {FragmentMapModule.class})
    abstract MapFragment provideMapFragment();

    @ContributesAndroidInjector(modules = {FragmentChartModule.class})
    abstract ChartFragment provideChartFragment();

}
