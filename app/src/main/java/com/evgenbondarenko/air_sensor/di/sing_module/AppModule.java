package com.evgenbondarenko.air_sensor.di.sing_module;

import android.content.Context;

import androidx.room.Room;

import com.evgenbondarenko.air_sensor.App;
import com.evgenbondarenko.air_sensor.ConstantsApp;
import com.evgenbondarenko.air_sensor.data.gps.GpsContract;
import com.evgenbondarenko.air_sensor.data.gps.GpsLocation;
import com.evgenbondarenko.air_sensor.data.Repository;
import com.evgenbondarenko.air_sensor.data.RepositoryContract;
import com.evgenbondarenko.air_sensor.data.SharedPreferencesStorage;
import com.evgenbondarenko.air_sensor.data.network_check.INetworkCheck;
import com.evgenbondarenko.air_sensor.data.network_check.NetworkCheck;
import com.evgenbondarenko.air_sensor.data.room.LocalEntryApi;
import com.evgenbondarenko.air_sensor.data.room.LocalEntrySensor;
import com.evgenbondarenko.air_sensor.data.room.SensorDao;
import com.evgenbondarenko.air_sensor.presentation.Router;
import com.evgenbondarenko.air_sensor.presentation.RouterContract;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class AppModule {
    @Singleton
    @Provides
    static Context provideContext(App app){return app;}

    @Singleton
    @Provides
    static SharedPreferencesStorage provideSharedPreferencesStorage(Context context){
        return new SharedPreferencesStorage(context);
    }

    @Singleton
    @Provides
    static SensorDao provideRoomDataBase(Context context) {
        return Room.databaseBuilder(
                context.getApplicationContext(), SensorDao.class, ConstantsApp.DAO_NAME)
                .allowMainThreadQueries()
                .build();
    }

    @Singleton
    @Provides
    static RepositoryContract provideRepository(Repository repository){
        return repository;
    }

    @Singleton
    @Provides
    static LocalEntryApi provideLocaleDaoApi(SensorDao dao) {
        return dao.getDaoApi();
    }

    @Singleton
    @Provides
    static LocalEntrySensor provideLocaleDaoSensor(SensorDao dao) {
        return dao.getDaoSensor();
    }

    @Singleton
    @Provides
    static INetworkCheck provideNetworkCheck(App app){
        return new NetworkCheck(app);
    }

    @Singleton
    @Provides
    static RouterContract provideRouter(Router router){
        return router;
    }

    @Singleton
    @Provides
    static GpsContract provideGps(GpsLocation gsp){
        return gsp;
    }
}
