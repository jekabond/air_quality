package com.evgenbondarenko.air_sensor.presentation.activity.splash;

import androidx.annotation.Nullable;
import android.os.Bundle;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.data.gps.GpsContract;
import com.evgenbondarenko.air_sensor.data.model.api.Time;
import com.evgenbondarenko.air_sensor.databinding.ActivitySplashBinding;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivity;
import com.evgenbondarenko.air_sensor.util.PermissionsUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashActivity extends BaseActivity<ActivitySplashBinding> implements SplashContract.View {

    @Inject SplashContract.Presenter presenter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    protected void createActivity(@Nullable Bundle savedInstanceState) {
        getBinding().setEvent(presenter);
        presenter.startView();
        PermissionsUtils.accessLocation(this)
                .flatMap(v -> v ? Single.just(true) : PermissionsUtils.accessLocation(this))
                .delay(1, TimeUnit.SECONDS)
                .flatMap(v -> presenter.isGps(v))
                .subscribe(new DisposableSingleObserver<Boolean>() {
                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        if(!aBoolean)toast("без Gps многие сервисы не доступны ");
                        presenter.start();
                        dispose();
                    }
                    @Override
                    public void onError(Throwable e) {
                        Timber.e("PermissionsUtils accessLocation error %s", e.getMessage());
                    }
                });

    }



    @Override
    protected void stopActivity() {

    }

    @Override
    protected void startActivity() {

    }

    @Override
    protected void pauseActivity() {

    }

    @Override
    protected void resumeActivity() {

    }

    @Override
    protected void destroyActivity() {
        presenter.stopView();


    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity,cycleFinish,object);
    }

    @Override
    public void transactionFragment(DaggerFragment daggerFragment) {

    }
}
