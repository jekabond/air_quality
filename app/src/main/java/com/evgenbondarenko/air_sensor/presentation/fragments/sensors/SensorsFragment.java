package com.evgenbondarenko.air_sensor.presentation.fragments.sensors;

import android.graphics.Color;
import android.os.Bundle;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.databinding.FragmentSensorsBinding;
import com.evgenbondarenko.air_sensor.presentation.base.BaseFragment;

import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

public class SensorsFragment extends BaseFragment<FragmentSensorsBinding> implements SensorsContract.View {
    @Inject SensorsContract.Presenter presenter;

    @Inject
    public SensorsFragment() {
        Timber.d("debug SensorsFragment");
    }

    public static SensorsFragment newInstance() {
        SensorsFragment fragment = new SensorsFragment();
        return fragment;
    }

    @Override
    public void pm2_5(Double value) {
        getBinding().pm25Value.setText(convertDoubleToString(value));
        getBinding().pm25Progress.setProgress(value.floatValue()/100);
    }

    @Override
    public void pm5(Double value)
    {
        getBinding().pm5Value.setText(convertDoubleToString(value));
        getBinding().pm5Progress.setProgress(value.floatValue()/100);
    }

    @Override
    public void pm10(Double value) {
        getBinding().pm10Value.setText(convertDoubleToString(value));
        getBinding().pm10Progress.setProgress(value.floatValue()/100);
    }

    private String convertDoubleToString(Double x){
        return String.format(Locale.US,"%.2f", x);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_sensors;
    }

    @Override
    protected void initFragmentView() {
        Timber.d("debug SensorsFragment initFragmentView");
        int[] colorList = new int[]{Color.GREEN, Color.YELLOW, Color.RED};
        getBinding().pm25Progress.applyGradient(colorList);
        getBinding().pm5Progress.applyGradient(colorList);
        getBinding().pm10Progress.applyGradient(colorList);
    }

    @Override
    protected void attachFragment() {
    }

    @Override
    protected void startFragment() {
        presenter.init();
    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {
        Timber.e("destroyFragment ");
    }

    @Override
    protected void pauseFragment() {
        presenter.dispose();
    }
}
