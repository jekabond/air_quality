package com.evgenbondarenko.air_sensor.presentation.activity.main;

import android.content.IntentFilter;
import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.util.CheckStatusReceiver;
import com.evgenbondarenko.air_sensor.presentation.base.BasePresenter;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;

public interface MainContract {

    interface View extends BaseActivityContract {
        void showSettingsDialog();

        void checkInternet(boolean check, boolean toast);

        void checkGPS(boolean check);

        void checkInternet(boolean check);

        void registerReceiverMain(CheckStatusReceiver receiver, IntentFilter filter);

        void unregisterReceiverMain(CheckStatusReceiver receiver);
    }

    interface Presenter extends BasePresenter {
        void init();

        void topMenuButtonClick();

        void bottomMenuButtonClick(MenuItem menuItem);

        void onDestroyView();
    }
}
