package com.evgenbondarenko.air_sensor.presentation.fragments.settings;

import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.databinding.FragmentSettingsBinding;

import javax.inject.Inject;

import dagger.android.support.DaggerDialogFragment;

public class SettingsFragment extends DaggerDialogFragment implements SettingsContract.View {
    private FragmentSettingsBinding binding;
    private WifiManager wifiManager;
    private LocationManager locationManager;

    @Inject
    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container,false);
        return binding.getRoot();



    }

    @Override
    public void changedWifi(boolean check) {
        if (check == true){
            binding.switch1.setChecked(true);
        } else binding.switch1.setChecked(false);
    }

    @Override
    public void changedGsm(boolean check) {

    }

    @Override
    public void changedGps(boolean check) {

    }
}
