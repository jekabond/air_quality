package com.evgenbondarenko.air_sensor.presentation.base;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.util.Objects;

import dagger.android.support.DaggerFragment;

public abstract class BaseFragment<Binding extends ViewDataBinding> extends DaggerFragment {
    private Binding binding;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        attachFragment();
    }

    @Override
    public void onStop() {
        stopFragment();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public void onPause() {
        pauseFragment();
        super.onPause();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        initFragmentView();
        return binding.getRoot();
    }

    protected Binding getBinding() {
        return binding;
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void initFragmentView();

    protected abstract void attachFragment();

    protected abstract void startFragment();

    protected abstract void stopFragment();

    protected abstract void destroyFragment();

    protected abstract void pauseFragment();

    @Override
    public void onDestroy() {
        destroyFragment();
        super.onDestroy();
    }

    public void hideKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) Objects.requireNonNull(this.getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(this.getActivity().getWindow().getDecorView().getWindowToken(), 0);

    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(this.getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).showSoftInput(this.getActivity().getWindow().getDecorView(), InputMethodManager.SHOW_IMPLICIT);
    }

}

