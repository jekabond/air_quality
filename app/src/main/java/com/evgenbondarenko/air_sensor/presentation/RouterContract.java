package com.evgenbondarenko.air_sensor.presentation;

import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;

public interface RouterContract {
    void startMainActivity();

    void startView(BaseActivityContract view);

    void stopView();

    void transactionSensorFragment();

    void openFragment(MenuItem menuItem);

    void  onDestroy();
}
