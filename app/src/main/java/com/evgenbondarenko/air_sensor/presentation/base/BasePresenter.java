package com.evgenbondarenko.air_sensor.presentation.base;

public interface BasePresenter {
        void startView();

        void stopView();

}
