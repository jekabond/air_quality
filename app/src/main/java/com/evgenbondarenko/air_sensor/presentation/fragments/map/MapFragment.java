package com.evgenbondarenko.air_sensor.presentation.fragments.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.data.exception.ExceptionAir;
import com.evgenbondarenko.air_sensor.data.gps.GpsContract;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import timber.log.Timber;

import static android.content.Context.LOCATION_SERVICE;
import static com.evgenbondarenko.air_sensor.ConstantsApp.INTERVAL_POSITION_MAP;

public class MapFragment extends DaggerFragment
        implements MapContract.View,
        OnMapReadyCallback {
    private SupportMapFragment mapFragment;
    private Observable<Location> locationObservable;
    private BehaviorSubject<Location> nextLocation;
    private CompositeDisposable disposable;
    private GoogleMap map;
    private LocationManager locationManager;
    private Criteria criteria;


    @Inject
    GpsContract gps;

    @Inject
    public MapFragment() {

    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        disposable = new CompositeDisposable();
        nextLocation = BehaviorSubject.create();
        locationObservable = gps.getEventLocation();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("MissingPermission")
    @Override
    public void onResume() {
        super.onResume();
        disposable.add(locationObservable
                .subscribeOn(Schedulers.io())
                .flatMapSingle(v -> gps.isLocation())
                .filter(isLocation -> isLocation)
                .flatMap(isLocation -> gps.getEventLocation())
                .map(location -> new LatLng(location.getLatitude(), location.getLongitude()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(latLng -> {
                            if (map == null) {
                                mapFragment.getMapAsync(this);
                            } else {
                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f));
                            }
                        },
                        throwable -> Timber.e("map gps error %s", throwable.getMessage())));
//        disposable.add(locationObservable
//                .subscribeOn(Schedulers.io())
//                .flatMap(v -> {
//                    if (criteria == null) criteria = new Criteria();
//                    if (locationManager != null) {
//                        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                        if (location == null) {
//                            return Observable.empty();
//                        } else {
//                            return Observable.just(new LatLng(location.getLatitude(), location.getLongitude()));
//                        }
//                    } else {
//                        if (getContext() != null) {
//                            locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
//                            Location location;
//                            if (locationManager != null) {
//                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                            } else {
//                                return Observable.empty();
//                            }
//                            if (location == null) {
//                                return Observable.empty();
//                            } else if (location.getLatitude() != 0D
//                                    && location.getLongitude() != 0D) {
//                                return Observable.just(new LatLng(location.getLatitude(), location.getLongitude()));
//                            } else {
//                                return Observable.empty();
//                            }
//                        } else {
//                            return Observable.empty();
//                        }
//                    }
//                })
//                .filter(v -> v.latitude != 0D && v.longitude != 0D)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(v -> {
//                            if (map == null) {
//                                mapFragment.getMapAsync(this);
//                            } else {
//                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(v, 18f));
//                            }
//                        },
//                        throwable -> Timber.e("map gps error %s", throwable.getMessage())));
    }

    @Override
    public void onDestroy() {
        if (disposable != null) {
            disposable.clear();
        }
        if (mapFragment != null) mapFragment = null;
        nextLocation.onComplete();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        disposable.clear();
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, null, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            this.mapFragment = mapFragment;
            mapFragment.getMapAsync(this);
        }
        return view;
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //googleApiClient.connect();
        //TileProvider tileProvider;
        map = googleMap;

//        if (map == null) {
//            if (getContext() != null && getActivity() != null &&
//                    ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
//                            PackageManager.PERMISSION_GRANTED) {
//                map = googleMap;
//                locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
//                map.setMyLocationEnabled(true);
//                map.setOnMyLocationButtonClickListener(() -> false);
//                map.setOnCameraMoveListener(() -> {
//
//                });
//                map.setOnMyLocationClickListener(location -> {
//
//                });
//                Timber.d("debug MapFragment onMapReady");
//                map.setMinZoomPreference(10f);
//                if (criteria == null) {
//                    criteria = new Criteria();
//                } else {
//                    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                    if (location != null) {
//                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 10f));
//                    }
//                }
//            }
//        } else {
//            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            if (location != null) {
//                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 10f));
//
//            }
//        }
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);
        Single<Location> single = gps.isLocation().subscribeOn(Schedulers.io())
                .flatMapObservable(flag -> {
                    if (flag) {
                        return gps.getEventLocation();
                    } else {

                        return Observable.empty();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(this::updateMap)
                .lastOrError();
        single.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.tag("onMapReady").e("exception updateMap %s", e.getMessage());
                    }
                });
    }

    public Observable<Location> updateMap(Location location) {
        if (map == null && location == null) {
            return Observable.empty();
        }
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(),
                location.getLongitude())).zoom(12).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        map.addTileOverlay(new TileOverlayOptions().tileProvider((new UrlTileProvider(128, 128) {
            @Override
            public URL getTileUrl(int x, int y, int zoom) {
                String s = "https://tiles.waqi.info/tiles/usepa-aqi/" + zoom + "/" + x + "/" + y + ".png";
                try {
                    return new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
            }
        })));
        return Observable.defer(() -> Observable.just(location));
    }


}
