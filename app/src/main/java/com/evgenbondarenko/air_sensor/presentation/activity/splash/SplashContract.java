package com.evgenbondarenko.air_sensor.presentation.activity.splash;

import android.text.BoringLayout;

import com.evgenbondarenko.air_sensor.presentation.base.BasePresenter;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;

import io.reactivex.Single;

public interface SplashContract {

    interface View extends BaseActivityContract {

    }

    interface Presenter extends BasePresenter{
        void start();

        Single<Boolean> isGps(boolean flag);
    }


}
