package com.evgenbondarenko.air_sensor.presentation.activity.main;

import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.data.room.EntryApi;
import com.evgenbondarenko.air_sensor.util.CheckStatusReceiver;
import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.network_check.INetworkCheck;
import com.evgenbondarenko.air_sensor.domain.main.MainInteractorContract;
import com.evgenbondarenko.air_sensor.presentation.RouterContract;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class MainPresenter implements MainContract.Presenter {
    private CompositeDisposable disposable;

    private CheckStatusReceiver receiver;

    @Inject
    MainContract.View view;

    @Inject
    MainInteractorContract interactor;

    @Inject
    RouterContract router;

    @Inject
    INetworkCheck networkCheck;

    @Inject
    MainPresenter() {
        disposable = new CompositeDisposable();
    }

    @Override
    public void startView() {
        disposable.add(interactor.locationApi()
                .subscribe(pair -> {
                    Timber.e("EventApi = %s Response %s",pair.getEntry(),pair.getResponse());
                },throwable -> Timber.tag("presenter locationApi").e("error -> %s",throwable.getMessage())));
        receiver = new CheckStatusReceiver();
        IntentFilter intentFilter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        receiver.onStart();
        view.registerReceiverMain(receiver,intentFilter);
        checkGPS();
        checkInternet();
    }



    @Override
    public void init() {
        router.stopView();
        router.startView(view);
        router.transactionSensorFragment();
    }

    @Override
    public void topMenuButtonClick() {
        Timber.d("menu button click");
        view.showSettingsDialog();
    }

    @Override
    public void bottomMenuButtonClick(MenuItem menuItem) {
        router.openFragment(menuItem);
    }

    @Override
    public void onDestroyView() {
        receiver.onStop();
        router.onDestroy();
        router.stopView();
        view.unregisterReceiverMain(receiver);
    }


    private void checkGPS() {
        Observable<Boolean> listener = receiver.getIsGpsEnabled()
                .subscribeOn(Schedulers.io());
        disposable.add(listener.observeOn(AndroidSchedulers.mainThread())
                .subscribe(v->view.checkGPS(v)
                        ,throwable ->
                                Timber.tag("presenter checkGPS")
                                .e("error -> %s",throwable.getMessage())));

    }


    private void checkInternet() {
        Observable<Boolean> listener = receiver.getIsNetworkEnabled()
                .subscribeOn(Schedulers.io());
        disposable.add(listener.observeOn(AndroidSchedulers.mainThread())
                .subscribe(v->view.checkInternet(v)
                        ,throwable ->
                                Timber.tag("presenter checkInternet")
                                        .e("error -> %s",throwable.getMessage())));
    }


    @Override
    public void stopView() {
        disposable.clear();
    }
}
