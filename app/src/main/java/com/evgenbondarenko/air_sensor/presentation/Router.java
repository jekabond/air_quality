package com.evgenbondarenko.air_sensor.presentation;

import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainActivity;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

@Singleton
public class Router implements RouterContract {
    private BaseActivityContract baseView;
    private SensorsFragment sensorsFragment = SensorsFragment.newInstance();
    private MapFragment mapFragment = MapFragment.newInstance();
    private ChartFragment chartFragment = ChartFragment.newInstance();

    @Inject
    Router() {

    }


    @Override
    public void startMainActivity() {
        baseView.transactionActivity(MainActivity.class, true);
    }

    @Override
    public void startView(BaseActivityContract view) {
        this.baseView = view;
    }

    @Override
    public void stopView() {
        if (baseView != null) baseView = null;
    }


    public void transactionSensorFragment(){
        if(baseView instanceof MainActivity){
            baseView.transactionFragment(sensorsFragment);
            Timber.e("sensorsFragment %s", sensorsFragment.isDetached());
        }
    }

    @Override
    public void openFragment(MenuItem menuItem) {
        Timber.d("debug Router openFragment %s",menuItem);
        switch (menuItem.getItemId()){
            case R.id.action_sensors: baseView.transactionFragment(sensorsFragment);
            break;
            case R.id.action_map: baseView.transactionFragment(mapFragment);
            break;
            case R.id.action_chart: baseView.transactionFragment(chartFragment);
            break;
        }
    }

    @Override
    public void onDestroy() {
        mapFragment.onDestroy();
        sensorsFragment.onDestroy();
        chartFragment.onDestroy();
    }
}
