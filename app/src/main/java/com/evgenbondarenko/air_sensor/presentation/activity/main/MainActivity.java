package com.evgenbondarenko.air_sensor.presentation.activity.main;

import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.evgenbondarenko.air_sensor.data.gps.GpsContract;
import com.evgenbondarenko.air_sensor.util.CheckStatusReceiver;
import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.databinding.ActivityMainBinding;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivity;
import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsFragment;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import timber.log.Timber;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {
    @Inject
    MainContract.Presenter mainPresenter;
    @Inject
    SettingsFragment settingsFragment;
    @Inject
    GpsContract gps;


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void createActivity(@Nullable Bundle savedInstanceState) {
        getBinding().setEvent(mainPresenter);
        getBinding().customActionBar.menuButton.setOnClickListener(v -> mainPresenter.topMenuButtonClick());
        getBinding().bottomNavigation.setOnNavigationItemSelectedListener(menuItem -> {
            mainPresenter.bottomMenuButtonClick(menuItem);
            return true;
        });
        getBinding().bottomNavigation.setSelectedItemId(R.id.action_sensors);
        mainPresenter.init();

    }

    @Override
    protected void stopActivity(){
        mainPresenter.stopView();
    }


    @Override
    protected void startActivity() {
       mainPresenter.startView();
    }


    @Override
    protected void pauseActivity() {

    }


    @Override
    protected void resumeActivity() {

    }

    @Override
    protected void destroyActivity() {
        mainPresenter.onDestroyView();
        gps.onStop();
    }


    @Override
    public void showSettingsDialog() {
//        toast("Настройки");
        settingsFragment.show(getSupportFragmentManager(), "settings");
    }

    @Override
    public void checkInternet(boolean check, boolean toast) {
//        if (check) {
//            getBinding().customActionBar.iconInternet.setColorFilter(getResources().getColor(R.color.lightOrangeColor));
//        } else {
//            if (toast)
//                toast(getResources().getString(R.string.not_internet));
//                getBinding().customActionBar.iconInternet.setColorFilter(getResources().getColor(R.color.lightBlueGrayColor));
//            }
//    }
//
//    @Override
//    public void checkGPS(boolean check) {
//        if (check == true){
//            getBinding().customActionBar.iconGps.setColorFilter(getResources().getColor(R.color.lightOrangeColor));
//        } else {
//            getBinding().customActionBar.iconGps.setColorFilter(getResources().getColor(R.color.lightBlueGrayColor));
//        }
//
//
    }

    @Override
    public void checkGPS(boolean check) {
        if (check) {
            getBinding().customActionBar.iconGps.setColorFilter(getResources().getColor(R.color.lightOrangeColor));
        } else {
            toast(getResources().getString(R.string.not_internet));
            getBinding().customActionBar.iconGps.setColorFilter(getResources().getColor(R.color.lightBlueGrayColor));
        }
    }

    @Override
    public void checkInternet(boolean check) {
        if (check) {
            getBinding().customActionBar.iconInternet.setColorFilter(getResources().getColor(R.color.lightOrangeColor));
        } else {
            toast(getResources().getString(R.string.not_internet));
            getBinding().customActionBar.iconInternet.setColorFilter(getResources().getColor(R.color.lightBlueGrayColor));
        }
    }

    @Override
    public void registerReceiverMain(CheckStatusReceiver receiver, IntentFilter filter) {
        registerReceiver(receiver, filter);
    }

    @Override
    public void unregisterReceiverMain(CheckStatusReceiver receiver) {
        unregisterReceiver(receiver);
    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity, cycleFinish, object);
    }

    @Override
    public void transactionFragment(DaggerFragment daggerFragment) {
        Timber.d("debug MainActivity transactionFragment " + daggerFragment);
        super.transactionFragmentNoBackStack(daggerFragment, R.id.content_main);
    }

    //private String convertDoubleToString(Double x){
//        return String.format(Locale.US,"%.2f", x);
//    }

}

