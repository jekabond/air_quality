package com.evgenbondarenko.air_sensor.presentation.fragments.settings;

public interface SettingsContract {
    interface View{
        void changedWifi(boolean check);
        void changedGsm(boolean check);
        void changedGps(boolean check);
    }

    interface Presenter{
        void onCheckedChanged(boolean check);
    }
}
