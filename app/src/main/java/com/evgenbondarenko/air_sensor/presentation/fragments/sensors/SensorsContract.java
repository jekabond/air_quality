package com.evgenbondarenko.air_sensor.presentation.fragments.sensors;

import com.evgenbondarenko.air_sensor.presentation.base.BasePresenter;

public interface SensorsContract {
    interface View {
        void pm2_5(Double value);

        void pm5(Double value);

        void pm10(Double value);
    }

    interface Presenter extends BasePresenter {
        void init();
        void dispose();
    }
}
