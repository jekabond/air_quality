package com.evgenbondarenko.air_sensor.presentation.fragments.sensors;

import com.evgenbondarenko.air_sensor.domain.main.MainInteractorContract;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;
import timber.log.Timber;

public class SensorsPresenter implements SensorsContract.Presenter{
    private CompositeDisposable disposable;


    @Inject MainInteractorContract interactor;
    @Inject SensorsContract.View view;


    @Inject
    SensorsPresenter() {
        Timber.d("init SensorsPresenter");
        disposable = new CompositeDisposable();
    }


    @Override
    public void init() {
       disposable.add(Observable.interval(50, TimeUnit.MILLISECONDS)
               .flatMap(v -> interactor.sensorEvent())
               .subscribe(v -> {
                   view.pm2_5(v.getPm2_5());
                   view.pm10(v.getPm10());
               },throwable -> Timber.tag("SensorsPresenter").e("init  error %s",throwable.getMessage())));
    }

    @Override
    public void dispose() {
        disposable.dispose();

    }

    @Override
    public void startView() {

    }

    @Override
    public void stopView() {

    }
}
