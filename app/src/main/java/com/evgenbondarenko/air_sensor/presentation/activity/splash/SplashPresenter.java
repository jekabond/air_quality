package com.evgenbondarenko.air_sensor.presentation.activity.splash;

import com.evgenbondarenko.air_sensor.data.gps.GpsContract;
import com.evgenbondarenko.air_sensor.presentation.RouterContract;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashPresenter implements SplashContract.Presenter {

    @Inject
    SplashContract.View view;
    @Inject
    RouterContract router;
    @Inject
    GpsContract gps;

    @Inject
    public SplashPresenter() {

    }

    @Override
    public void start(){
        router.startMainActivity();
    }

    @Override
    public Single<Boolean> isGps(boolean flag) {
            if (flag) {
                gps.onStart();
            }
            return Single.just(flag);
    }

    @Override
    public void startView() {
        router.startView(view);
    }

    @Override
    public void stopView() {
        if(view != null) view = null;
    }
}
