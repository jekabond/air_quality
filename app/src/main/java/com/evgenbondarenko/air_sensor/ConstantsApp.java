package com.evgenbondarenko.air_sensor;

public abstract class ConstantsApp {
    public final static String DAO_NAME = "sensor_dao";
    public final static String LOG_FILE_NAME ="logDebug";
    public final static long UPDATE_INTERVAL = 60_000;
    public final static long FASTEST_INTERVAL = 60_000;
    public final static long INTERVAL_POSITION_MAP = 20000L;
}
