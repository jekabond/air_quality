package com.evgenbondarenko.air_sensor.domain.main;

import com.evgenbondarenko.air_sensor.data.gps.Distance;
import com.evgenbondarenko.air_sensor.data.gps.GpPoint;
import com.evgenbondarenko.air_sensor.data.gps.GpsContract;
import com.evgenbondarenko.air_sensor.data.RepositoryContract;
import com.evgenbondarenko.air_sensor.data.SharedPreferencesStorage;
import com.evgenbondarenko.air_sensor.data.model.Pair;
import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.room.EntryApi;
import com.evgenbondarenko.air_sensor.data.sensor_mock.Sensor;
import com.evgenbondarenko.air_sensor.data.usb_driver.UsbDriverContract;
import com.evgenbondarenko.air_sensor.domain.BaseInteractor;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MainInteractor extends BaseInteractor implements MainInteractorContract {

    @Inject
    UsbDriverContract sensor;
    @Inject
    RepositoryContract repository;
    @Inject
    SharedPreferencesStorage pref;
    @Inject
    GpsContract gps;
    @Inject
    Distance distance;

    @Inject
    public MainInteractor() {

    }


    @Override
    public Observable<Pair<EntryApi, Response>> locationApi() {
        return gps.getEventGps()
                .flatMap(gps -> repository.getCount().toObservable()
                        .flatMap(count -> filterData(gps, count)))
                .flatMapSingle(entry -> repository.locationApi(Double.parseDouble(entry.getLatitude()),
                        Double.parseDouble(entry.getLongitude()))
                        .flatMap(response -> Single.just(new Pair<>(entry, response))))
                .flatMapSingle(this::updateEntry)
                .filter(v -> v.getEntry().getId() != 0 && v.getResponse() != null)
                .flatMap(Observable::just);
    }


    private Observable<EntryApi> filterData(LatLng gps, int count) {
        EntryApi entryApi = new EntryApi();
        entryApi.setLatitude(String.valueOf(gps.latitude));
        entryApi.setLongitude(String.valueOf(gps.longitude));
        if (count == 0) {
            entryApi.setId(repository.insertEntry(entryApi));
            return Observable.just(entryApi);
        } else {
            return repository.getListEntryApi().toObservable()
                    .flatMap(list -> {
                        EntryApi entryApi1 = checkListApi(list, entryApi);
                        if (entryApi1.getId() == 0) {
                            entryApi.setId(repository.insertEntry(entryApi));
                        }
                        return Observable.just(entryApi);
                    });
        }
    }

    private Single<Pair<EntryApi, Response>> updateEntry(Pair<EntryApi, Response> pair) {
        pair.getEntry().setPm2(pair.getResponse().getData().getIaqi().getPm25().getV());
        pair.getEntry().setPm10(pair.getResponse().getData().getIaqi().getPm10().getV());
        pair.getEntry().setId(repository.updateEntryApi(pair.getEntry()));
        return Single.just(pair);
    }

    private EntryApi checkListApi(List<EntryApi> list, EntryApi entry) {
        if (list.size() > 0) {
            Map<Double, EntryApi> buffer = new HashMap();
            for (int i = 0; i < list.size(); i++) {
                double dis = distance.doDistance(new GpPoint(
                                Double.parseDouble(entry.getLatitude()),
                                Double.parseDouble(entry.getLongitude())
                        ),
                        new GpPoint(
                                Double.parseDouble(list.get(i).getLatitude()),
                                Double.parseDouble(list.get(i).getLongitude()))
                );
                if (dis < 1_000) {
                    buffer.put(dis, list.get(i));
                }
            }
            if (buffer.size() > 0) {
                Map<Double, EntryApi> sortedEntry = new TreeMap<>(Double::compareTo);
                sortedEntry.putAll(buffer);
                if (sortedEntry.size() > 0 && new ArrayList<>(sortedEntry.values()).get(0) != null) {
                    EntryApi tmp = new ArrayList<>(sortedEntry.values()).get(0);
                    Double key = new ArrayList<>(sortedEntry.keySet()).get(0);
                    entry.setLatitude(tmp.getLongitude());
                    entry.setLongitude(tmp.getLatitude());
                    entry.setId(tmp.getId());
                    entry.setPm2(tmp.getPm2());
                    entry.setPm10(tmp.getPm10());
                    sortedEntry.remove(key);
                }
                if (sortedEntry.size() > 0)repository.deleteListApi(new ArrayList<>(sortedEntry.values()));
                return entry;
            } else {
                return entry;
            }
        } else {
            return entry;
        }
    }


    @Override
    public Observable<Sensor> sensorEvent() {
        return  Observable.defer(() -> sensor.getSensorData())
                .filter(f -> f.getPm2_5() != 0D && f.getPm10() != 0D)
                .compose(applyObservableSchedulers());
    }

}
