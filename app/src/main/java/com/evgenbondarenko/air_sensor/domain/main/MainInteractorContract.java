package com.evgenbondarenko.air_sensor.domain.main;

import com.evgenbondarenko.air_sensor.data.model.Pair;
import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.room.EntryApi;
import com.evgenbondarenko.air_sensor.data.sensor_mock.Sensor;

import io.reactivex.Observable;

public interface MainInteractorContract {
    Observable<Pair<EntryApi, Response>> locationApi();

    Observable<Sensor> sensorEvent();

}
