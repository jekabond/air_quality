package com.evgenbondarenko.air_sensor.data.usb_driver;


import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

import com.evgenbondarenko.air_sensor.data.sensor_mock.Sensor;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class UsbDriver implements UsbDriverContract {
    private UsbManager manager;
    @Inject
    Context context;


    @Inject
    public UsbDriver() {

    }

    @Inject
    public void init(){
        manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
    }


    @Override
    public Observable<Sensor> getSensorData() {
        Sensor sensor = new Sensor();
        return Observable.defer(() -> {
                if (manager == null) {
                    return Observable.empty();
                }
                List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);

                if (availableDrivers.isEmpty()) {
                    return Observable.empty();
                }
                UsbSerialDriver driver = availableDrivers.get(0);
                UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
                if (connection == null) {
                    return Observable.empty();
                }
                UsbSerialPort port = driver.getPorts().get(0);
                port.open(connection);
                port.setParameters(9600, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                byte[] buffer = new byte[16];

                int numBytesRead = port.read(buffer, 5000);
                port.close();
                if (numBytesRead == 10) {
                    int check = ((Byte) buffer[8]).intValue();
                    int pm5h = ((Byte) buffer[3]).intValue();
                    int pm5l = ((Byte) buffer[2]).intValue();
                    int pm10l = ((Byte) buffer[4]).intValue();
                    int pm10h = ((Byte) buffer[5]).intValue();
                    int res1 = ((Byte) buffer[6]).intValue();
                    int res2 = ((Byte) buffer[7]).intValue();

                    if (check == res1 + res2 + pm5h + pm5l + pm10h + pm10l) {
                        int pm5 = ((pm5h * 256) + pm5l) / 10;
                        int pm10 = ((pm10h * 256) + pm10l) / 10;
                        pm5 = pm5 < 0 ? 0 : pm5;
                        pm10 = pm10 < 0 ? 0 : pm10;
                        sensor.setPm2_5(pm5);
                        sensor.setPm10(pm10);
                    } else {
                        return Observable.empty();
                    }
                }
            return Observable.just(sensor);
        }).subscribeOn(Schedulers.io());
    }
}

