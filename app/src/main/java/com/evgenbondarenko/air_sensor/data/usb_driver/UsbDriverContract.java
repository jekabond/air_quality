package com.evgenbondarenko.air_sensor.data.usb_driver;

import com.evgenbondarenko.air_sensor.data.sensor_mock.Sensor;

import io.reactivex.Observable;

public interface UsbDriverContract {

    Observable<Sensor> getSensorData();
}
