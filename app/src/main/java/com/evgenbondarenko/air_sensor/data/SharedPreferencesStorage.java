package com.evgenbondarenko.air_sensor.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.evgenbondarenko.air_sensor.ConstantsApp;

import javax.inject.Inject;
import javax.inject.Singleton;

import static android.content.Context.MODE_PRIVATE;

@Singleton
public class SharedPreferencesStorage {
    private final SharedPreferences pref;
    private final Editor editor;

    @SuppressLint("CommitPrefEdits")
    @Inject
    public SharedPreferencesStorage(Context context) {
        pref = context.getApplicationContext().getSharedPreferences(ConstantsApp.DAO_NAME, MODE_PRIVATE);
        editor = pref.edit();
    }



    public void saveDataString(String key,String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void saveDataBoolean(String key,boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void saveDataLong(String key, long value){
        editor.putLong(key, value);
        editor.commit();
    }

    public void saveDataDouble(String key, double value){
        editor.putString(key,String.valueOf(value));
    }

    public double loadDataDouble(String key) {
        String value = pref.getString(key, "");
        if (value != null && value.isEmpty()){
            value = "0.0";
        }else if (value == null){
            value = "0.0";
        }
        return Double.parseDouble(value);
    }

    public Boolean loadDataBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    public String loadDataString(String key) {
        return pref.getString(key, "");
    }

    public Long loadDataLong(String key) {
        return pref.getLong(key, 0L);
    }

    public void clearAll() {
        pref.edit().clear().apply();
    }
}
