package com.evgenbondarenko.air_sensor.data.gps;


import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class Distance {

    @Inject
    public Distance() {
    }

    public double doDistance(GpPoint gpA, GpPoint gpB) {
        int radius = 6_378_137;//6_378_100;//радиус земли в метрах 6371;// радиус земли в километрах
        double dLat = Math.toRadians(gpB.getLalitude() - gpA.getLalitude());
        double dLong = Math.toRadians(gpB.getLongitude() - gpA.getLongitude());
        double lat1 = Math.toRadians(gpA.getLalitude());
        double lat2 = Math.toRadians(gpB.getLalitude());
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return c * radius;
    }
}
