package com.evgenbondarenko.air_sensor.data;

import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.room.EntryApi;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface RepositoryContract {

    long insertEntry(EntryApi entryApi);

    Single<List<EntryApi>> getListEntryApi();

    int updateEntryApi(EntryApi entryApi);

    void deleteListApi(List<EntryApi> list);

    Single<Integer> getCount();

    Single<EntryApi> getLastPointApi();

    Flowable<EntryApi> getLastPointSensor();

    Flowable<List<EntryApi>> getListEntrySensor();

    Single<Response> locationApi(double lat, double lng);
}
