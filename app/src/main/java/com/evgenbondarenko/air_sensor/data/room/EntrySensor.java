package com.evgenbondarenko.air_sensor.data.room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "entrySensor")
public class EntrySensor {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String lotitude;
    private String longitude;
    private String adress;
    private String pm2;
    private String pm5;
    private String pm10;

    public EntrySensor() {

    }

    @Override
    public String toString() {
        return "EntryApi{" +
                "id=" + id +
                ", lotitude='" + lotitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", adress='" + adress + '\'' +
                ", pm2='" + pm2 + '\'' +
                ", pm5='" + pm5 + '\'' +
                ", pm10='" + pm10 + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLotitude() {
        return lotitude;
    }

    public void setLotitude(String lotitude) {
        this.lotitude = lotitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPm2() {
        return pm2;
    }

    public void setPm2(String pm2) {
        this.pm2 = pm2;
    }

    public String getPm5() {
        return pm5;
    }

    public void setPm5(String pm5) {
        this.pm5 = pm5;
    }

    public String getPm10() {
        return pm10;
    }

    public void setPm10(String pm10) {
        this.pm10 = pm10;
    }

}
