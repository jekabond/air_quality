package com.evgenbondarenko.air_sensor.data.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface LocalEntrySensor {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEntry(EntrySensor entryApi);

    @Query("SELECT * FROM entrySensor ORDER BY id DESC LIMIT 1;")
    Flowable<List<EntrySensor>> queryLastPointSensor();

    @Query("SELECT * FROM entrySensor")
    Single<List<EntrySensor>> queryEntryAll();

    @Query("SELECT * FROM entrySensor WHERE id IS :key")
    Single<EntrySensor> queryEntry(String key);
}
