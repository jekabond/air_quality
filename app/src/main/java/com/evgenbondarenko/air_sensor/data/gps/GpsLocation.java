package com.evgenbondarenko.air_sensor.data.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;

import androidx.core.app.ActivityCompat;

import com.evgenbondarenko.air_sensor.data.model.api.Time;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

import static com.evgenbondarenko.air_sensor.ConstantsApp.FASTEST_INTERVAL;
import static com.evgenbondarenko.air_sensor.ConstantsApp.UPDATE_INTERVAL;


public class GpsLocation implements GpsContract {
    private BehaviorSubject<LatLng> eventGps;
    private BehaviorSubject<Location> eventLocation;
    private LocationRequest mLocationRequest;
    private SettingsClient settingsClient;
    private Location location;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationSettingsRequest.Builder builder;

    @Inject Context context;

    @Inject
    public GpsLocation() {

    }

    @Override
    public void onStart(){
        eventGps = BehaviorSubject.create();
        eventLocation = BehaviorSubject.create();
        Completable.fromAction(this::initGpsLocation)
        .subscribeOn(AndroidSchedulers.mainThread())
        .subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                dispose();
            }

            @Override
            public void onError(Throwable e) {
                Timber.e("");
            }
        });
    }

    @Override
    public void onStop(){
        if (eventGps != null){
            eventGps.onComplete();
            eventGps = null;
        }
        mLocationRequest = null;
        settingsClient = null;
        locationSettingsRequest = null;
        builder = null;
    }

    @Override
    public boolean isEnable(){
        if (eventGps != null){
            return true;
        }else {
            return false;
        }
    }


    private void initGpsLocation() {
        if (context != null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
            builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            locationSettingsRequest = builder.build();
            settingsClient = LocationServices.getSettingsClient(context);
            settingsClient.checkLocationSettings(locationSettingsRequest);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {
                LocationServices.getFusedLocationProviderClient(context.getApplicationContext()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        onLocationChanged(locationResult.getLastLocation());
                    }
                }, Looper.myLooper());
            }
        }
    }

    private void onLocationChanged(Location location) {
        if (location != null) {
            this.location = location;
            LatLng lng = new LatLng(location.getLatitude(), location.getLongitude());
            if (lng.latitude != 0D && lng.longitude != 0D) {
                eventGps.onNext(lng);
                eventLocation.onNext(location);
            }
        }
    }

    public Single<Boolean> isLocation(){
        return Single.defer(() -> {
            if (location == null){
                return Single.just(false);
            }else {
                return Single.just(true);
            }
        }).subscribeOn(Schedulers.io());
    }


    public Observable<Location> getEventLocation() {
        return eventLocation;
    }

    @Override
    public Observable<LatLng> getEventGps() {
        return eventGps;
    }
}
