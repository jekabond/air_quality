package com.evgenbondarenko.air_sensor.data.gps;

public class GpPoint {
    private double lalitude;
    private double longitude;

    public GpPoint(double lalitude, double longitude) {
        this.lalitude = lalitude;
        this.longitude = longitude;
    }

    public double getLalitude() {
        return lalitude;
    }

    public void setLalitude(double lalitude) {
        this.lalitude = lalitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
