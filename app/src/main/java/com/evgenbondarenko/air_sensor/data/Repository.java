package com.evgenbondarenko.air_sensor.data;

import com.evgenbondarenko.air_sensor.BuildConfig;
import com.evgenbondarenko.air_sensor.data.api_service.ApiService;
import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.room.EntryApi;
import com.evgenbondarenko.air_sensor.data.room.LocalEntryApi;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class Repository implements RepositoryContract {

    @Inject
    ApiService api;
    @Inject
    LocalEntryApi daoApi;


    @Inject
    public Repository() {
    }

    //Storage
    @Override
    public long insertEntry(EntryApi entryApi) {
        return daoApi.insertEntry(entryApi);
    }

    @Override
    public Single<List<EntryApi>> getListEntryApi() {
        return daoApi.getEntryListApi();
    }

    @Override
    public int updateEntryApi(EntryApi entryApi) {
        return daoApi.updateEntryApi(entryApi);
    }

    @Override
    public void deleteListApi(List<EntryApi> list) {
        daoApi.deleteListApi(list);
    }

    @Override
    public Single<Integer> getCount() {
        return daoApi.getCount();
    }

    @Override
    public Single<EntryApi> getLastPointApi() {
        return daoApi.getLastPointApi();
    }

    @Override
    public Flowable<EntryApi> getLastPointSensor() {
        return null;
    }

    @Override
    public Flowable<List<EntryApi>> getListEntrySensor() {
        return null;
    }

    //Api
    @Override
    public Single<Response> locationApi(double lat, double lng) {
        return api.queryApi(String.valueOf(lat).concat(";").concat(String.valueOf(lng)), BuildConfig.TOKEN)
                .subscribeOn(Schedulers.io())
                .doOnError(throwable -> Timber.e("api error -> %s", throwable.getMessage()));
    }


}
