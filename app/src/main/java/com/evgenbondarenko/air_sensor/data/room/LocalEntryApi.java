package com.evgenbondarenko.air_sensor.data.room;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface LocalEntryApi {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertEntry(EntryApi entryApi);


    @Query("SELECT * FROM EntryApi ORDER BY id DESC LIMIT 1;")
    Single<EntryApi> getLastPointApi();

    @Query("SELECT COUNT(*) FROM EntryApi")
    Single<Integer> getCount();

    @Update
    int updateEntryApi(EntryApi entryApi);

    @Delete
    void deleteListApi(List<EntryApi> list);

    @Query("SELECT * FROM entryApi")
    Single<List<EntryApi>> getEntryListApi();

    @Query("SELECT * FROM EntryApi WHERE id IS :key")
    Single<EntryApi> queryEntry(String key);
}
