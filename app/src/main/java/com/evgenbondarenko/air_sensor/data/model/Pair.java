package com.evgenbondarenko.air_sensor.data.model;

import java.util.Objects;

public class Pair<Entry, Response> {
    private Entry entry;
    private Response response;


    public Pair() {
    }

    public Pair(Entry entry, Response response) {
        this.entry = entry;
        this.response = response;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(entry, pair.entry) &&
                Objects.equals(response, pair.response);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entry, response);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "entry=" + entry +
                ", response=" + response +
                '}';
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
