package com.evgenbondarenko.air_sensor.data.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {EntryApi.class,EntrySensor.class},version = 2, exportSchema = false)
public abstract class SensorDao extends RoomDatabase {
    public  abstract LocalEntryApi getDaoApi();
    public  abstract LocalEntrySensor getDaoSensor();
}
