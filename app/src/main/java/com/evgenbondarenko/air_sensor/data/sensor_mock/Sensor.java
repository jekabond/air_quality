package com.evgenbondarenko.air_sensor.data.sensor_mock;


import java.util.Objects;

public class Sensor {
    private double pm2_5;
    private double pm10;

    public Sensor() {
    }

    public double getPm2_5() {
        return pm2_5;
    }

    public void setPm2_5(double pm2_5) {
        this.pm2_5 = pm2_5;
    }

    public double getPm10() {
        return pm10;
    }

    public void setPm10(double pm10) {
        this.pm10 = pm10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sensor sensor = (Sensor) o;
        return Double.compare(sensor.pm2_5, pm2_5) == 0 &&
                Double.compare(sensor.pm10, pm10) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pm2_5, pm10);
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "pm2_5=" + pm2_5 +
                ", pm10=" + pm10 +
                '}';
    }
}
