package com.evgenbondarenko.air_sensor.data.exception;

public class ExceptionAir extends Throwable {
    public final static String CANCEL = "cancel";
    public final static String LOCATION_EXCEPTION = "location_exception";

    public ExceptionAir(String message) {
        super(message);
    }
}
