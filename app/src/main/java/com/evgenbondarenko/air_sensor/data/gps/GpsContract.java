package com.evgenbondarenko.air_sensor.data.gps;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface GpsContract {
    Observable<LatLng> getEventGps();

    Single<Boolean> isLocation();

    Observable<Location> getEventLocation();

    void onStart();

    void onStop();

    boolean isEnable();
}
